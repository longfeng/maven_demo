package com.leon.learn.maven.bean;





import lombok.Data;

import java.io.Serializable;

/**
 * 一个bean
 */
@Data
public class TestBean implements Serializable {

    /** age xxx */
    private Integer age;
    /** name qwrwer */
    private String name;
}
