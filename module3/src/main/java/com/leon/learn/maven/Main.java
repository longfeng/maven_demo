package com.leon.learn.maven;

import cn.hutool.core.bean.BeanUtil;
import com.leon.learn.maven.bean.TestBean;

/**
 * 这是个main方法
 */
public class Main {

    public static void main(String[] args) {
        TestBean testBean = new TestBean();
        testBean.setAge(123123);
        testBean.setName("new test....");


        String result = new Main().func(testBean);
        System.err.println(result);
    }

    /**mvn
     * main方法的fun
     * @param testBean  好参数
     * @return 一个字符
     */
    private String func(TestBean testBean) {
        System.err.println(testBean.getAge());
        System.err.println(testBean.getName());
        System.err.println(BeanUtil.beanToMap(testBean));
        return testBean.toString();
    }

}
