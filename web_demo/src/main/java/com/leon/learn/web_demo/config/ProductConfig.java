package com.leon.learn.web_demo.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author zhen
 * @Date 2018/12/10 16:01
 */
@Configuration
@ComponentScan("com.leon.learn.web_demo")
@Import(SpringMVCConfig.class)
public class ProductConfig {

}