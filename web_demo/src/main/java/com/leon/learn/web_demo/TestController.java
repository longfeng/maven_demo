package com.leon.learn.web_demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/test")
public class TestController {

    @RequestMapping("/do")
    @ResponseBody
    public String test(){
        System.out.println("测试方法执行！");
        return "ok!";
    }
}
